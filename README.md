# To download

Visit [https://bitbucket.org/deeplocal/bia-email-template/downloads/](https://bitbucket.org/deeplocal/bia-email-template/downloads/) and click `Download repository`.

## For Changing Content

To replace things like image links, search for `<img`, and replace the contents of `src=""` with the hosted image that you want to use.

## For Development

1. Install Node & Git
1. `git clone git@bitbucket.org:deeplocal/bia-email-template.git`
1. `cd bia-email-invite`
1. `npm install`
1. `npm run start`
1. Open your browser to [http://localhost:9876](http://localhost:9876)
